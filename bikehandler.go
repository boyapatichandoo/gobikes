package gobikes

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/boyapatichandoo/goutil"
)

type bike struct {
	Name    string `json:"name"`
	Company string `json:"company"`
}

var bikesData = []bike{{Name: "CB300R", Company: "Honda"}, {Name: "Adventure", Company: "KTM"}, {Name: "CB650R", Company: "Honda"}, {Name: "CBR1000", Company: "Honda"}, {Name: "Pinagale", Company: "Ducati"}, {Name: "Street Triple", Company: "Truimph"}}

//GetBikes ...
func GetBikes(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	bikes, err := json.Marshal(goutil.StringSort(bikesData, func(i, j int) bool {
		return bikesData[i].Name > bikesData[j].Name
	}))
	if err != nil {
		log.Println("bikes", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
	w.Write(bikes)
}
